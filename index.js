KEY = "8377c58a65c70a55136c3d5e7a307263";
TOKEN =
  "ATTAecc811f9c54bc22e0097fb99f85ef0c58a16568cff4b31a10d8ec2dccc194fc1D16D013C";

function getBoard(boardId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}?key=${KEY}&token=${TOKEN}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function createBoard(boardName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/?name=${boardName}&key=${KEY}&token=${TOKEN}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        let boardId = data.id;
        return getBoard(boardId);
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getLists(boardId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}/lists?key=${KEY}&token=${TOKEN}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getCards(listId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists/${listId}/cards?key=${KEY}&token=${TOKEN}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getAllCards(boardId) {
  return new Promise((resolve, reject) => {
    getLists(boardId).then((lists) => {
      let cardsPromises = lists.map((list) => {
        let listId = list.id;
        return getCards(listId);
      });

      Promise.all(cardsPromises)
        .then((data) => {
          let cardsData = [];
          data.forEach((list) => {
            cardsData = cardsData.concat(list);
          });
          resolve(cardsData);
        })
        .catch((error) => {
          reject(error);
        });
    });
  });
}

function createList(boardId, listName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}/lists?name=${listName}&key=${KEY}&token=${TOKEN}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((data) => data.json())
      .then((data) => resolve(data))
      .catch((error) => reject(error));
  });
}

function createCard(listId, cardName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${KEY}&token=${TOKEN}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((data) => data.json())
      .then((data) => resolve(data))
      .catch((error) => reject(error));
  });
}

function createBoardListsAndCards(boardName) {
  return new Promise((resolve, reject) => {
    createBoard(boardName)
      .then((data) => {
        let boardId = data.id;
        let listPromises = [];

        for (let counter = 1; counter <= 3; counter++) {
          listPromises.push(createList(boardId, "list" + counter));
        }

        Promise.all(listPromises)
          .then((lists) => {
            let cardPromises = lists.map((list) => createCard(list.id, "card"));

            Promise.all(cardPromises)
              .then((cards) => resolve(cards))
              .catch((error) => reject(error));
          })
          .catch((error) => reject(error));
      })
      .catch((error) => reject(error));
  });
}

function deleteList(listId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${KEY}&token=${TOKEN}`,
      {
        method: "PUT",
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function deleteAllLists() {
  return new Promise((resolve, reject) => {
    createBoardListsAndCards("new_board").then((data) => {
      let deletePromises = data.map((list) => {
        return deleteList(list.idList);
      });

      Promise.all(deletePromises)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  });
}

function deleteAllListsSequentially() {
  return new Promise((resolve, reject) => {
    createBoardListsAndCards("new_board").then((data) => {
      data
        .reduce((accumulator, list) => {
          return accumulator.then(() => {
            deleteList(list.idList);
          });
        }, Promise.resolve())
        .then((data) => {
          resolve("All lists deleted successfully");
        })
        .catch((error) => {
          reject(error);
        });
    });
  });
}

function getCheckItems(checklistId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${KEY}&token=${TOKEN}`,
      {
        method: "GET",
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function updateAllCheckItems(cardId, checklistId) {
  return new Promise((resolve, reject) => {
    getCheckItems(checklistId).then((data) => {
      let checkItemPromises = data.map((checkItem) => {
        if (checkItem.state == "incomplete") {
          let checkItemId = checkItem.id;
          return fetch(
            `https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?state=complete&key=${KEY}&token=${TOKEN}`,
            {
              method: "PUT",
              headers: {
                Accept: "application/json",
              },
            }
          );
        }
      });

      Promise.all(checkItemPromises)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  });
}

function updateCheckItemToIncomplete(cardId, checkItemId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=incomplete&key=${KEY}&token=${TOKEN}`,
      { method: "PUT" }
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(`Updated CheckItem ${checkItemId} to incomplete`);
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function delay(timeInMilliseconds) {
  return new Promise((resolve) => setTimeout(resolve, timeInMilliseconds));
}

function updateAllCheckItemsToIncompleteSequentially(cardId, checklistId) {
  return new Promise((resolve, reject) => {
    getCheckItems(checklistId)
      .then((data) => {
        return data.reduce((accumulator, checkItem) => {
          return accumulator.then(() => {
            if (checkItem.state == "complete") {
              const checkItemId = checkItem.id;
              return updateCheckItemToIncomplete(cardId, checkItemId).then(() =>
                delay(1000)
              );
            } else {
              return Promise.resolve();
            }
          });
        }, Promise.resolve());
      })
      .then(() => resolve("All items updated successfully"))
      .catch((error) => reject(error));
  });
}

module.exports = {
  getBoard,
  createBoard,
  getLists,
  getCards,
  getAllCards,
  createBoardListsAndCards,
  deleteAllLists,
  deleteAllListsSequentially,
  updateAllCheckItems,
  updateAllCheckItemsToIncompleteSequentially,
};
