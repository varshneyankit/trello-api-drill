const { deleteAllLists } = require("../index");

deleteAllLists()
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error);
  });
