const { createBoardListsAndCards } = require("../index");

const boardName = "another_board";

createBoardListsAndCards(boardName)
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error);
  });
