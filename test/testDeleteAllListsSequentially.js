const { deleteAllListsSequentially } = require("../index");

deleteAllListsSequentially()
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error);
  });
