const { createBoard } = require("../index");

const boardName = "new_board";

createBoard(boardName)
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error);
  });
