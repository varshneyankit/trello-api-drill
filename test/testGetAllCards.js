const { getAllCards } = require("../index");

const boardId = "66541300a18cd4ddae4651cf";

getAllCards(boardId)
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error);
  });
