const { updateAllCheckItemsToIncompleteSequentially } = require("../index");

const cardId = "665413529317655267e6af97";
const checkListId = "6654bdc5b5dca5281d0a8d46";

updateAllCheckItemsToIncompleteSequentially(cardId, checkListId)
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error);
  });
